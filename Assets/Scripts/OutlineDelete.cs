﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OutlineDelete : MonoBehaviour {

    public void AnimEnd()
    {
        Destroy(gameObject, 0.2f);
    }
}
