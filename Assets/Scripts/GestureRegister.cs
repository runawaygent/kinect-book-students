﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Overly
{
    public class GestureRegister : MonoBehaviour
    {
        /// <summary>
        /// The page turner script
        /// </summary>
        [Tooltip("The page turner script")]
        public PageTurner turner;
        /// <summary>
        /// The time it takes to swipe a page
        /// </summary>
        [Tooltip("The time it takes to swipe a page")]
        public float swipeTime;
        /// <summary>
        /// The distance needed to swipe to turn a page
        /// </summary>
        [Tooltip("The distance needed to swipe to turn a page")]
        public float swipeDistance;
        /// <summary>
        /// Time to wait until accepting next swipe
        /// </summary>
        [Tooltip("Time to wait until accepting next swipe")]
        public float maxTimeOut;
        /// <summary>
        /// Maximum distance from the kinect in decimeters
        /// </summary>
        [Tooltip("Maximum distance from the kinect in decimeters")]
        public float maxDistance;

        /// <summary>
        /// List of currently registered bodies for tracking
        /// </summary>
        private List<Body> bodies = new List<Body>();
        /// <summary>
        /// Dictionary tracking body position for swiping
        /// Key: bodyID returned by kinect
        /// List holds distance from previous position and time since recording it
        /// </summary>
        private Dictionary<string, List<KeyValuePair<float, float>>> idTimes = new Dictionary<string, List<KeyValuePair<float, float>>>();
        /// <summary>
        /// Dictionary holding time since the body was registered for swiping
        /// </summary>
        private Dictionary<string, float> timeSince = new Dictionary<string, float>();
        /// <summary>
        /// Time since last page turn
        /// </summary>
        private float timeOut;

        /// <summary>
        /// Adds a body to tracking dict after it interracts with the page side arrow
        /// </summary>
        /// <param name="obj"></param>
        public void AddToId(GameObject obj)
        {
            string name = obj.name;
            string side;
            Body bod = null;
            if (name.Contains("Left"))
            {
                side = "Left";
                foreach(Body b in bodies)
                {
                    if(b.LeftWrist == obj)
                    {
                        bod = b;
                        break;
                    }
                }
            }
            else
            {
                side = "Right";
                foreach (Body b in bodies)
                {
                    if (b.RightWrist == obj)
                    {
                        bod = b;
                        break;
                    }
                }
            }

            if(bod == null)
            {
                Debug.LogError("Body not found");
                return;
            }

            if (!idTimes.ContainsKey(side + bod.bodyID))
                idTimes.Add(side + bod.bodyID, new List<KeyValuePair<float, float>>());

            if (timeSince.ContainsKey(side + bod.bodyID))
                timeSince[side + bod.bodyID] = 0;
            else
                timeSince.Add(side + bod.bodyID, 0);
        }

        // Update is called once per frame
        void Update()
        {
            
            foreach (Body b in bodies)
            {
                if (b.LeftElbow.transform.position.z < maxDistance)
                    AddList(b.LeftWrist.transform, b, b.prevLeftLocX, b.LeftWrist.transform.position.x, "Left");
                if (b.RightElbow.transform.position.z < maxDistance)
                    AddList(b.RightWrist.transform, b, b.prevRightLocX, b.RightWrist.transform.position.x, "Right");

                b.SetPositions();
            }

            if (timeOut > 0)
                timeOut -= Time.smoothDeltaTime;            
        }

        private int GetActiveBodies()
        {
            int count = 0;
            foreach(Body b in bodies)
            {
                if (b.LeftElbow.activeSelf)
                    count++;

                if (b.RightElbow.activeSelf)
                    count++;
            }

            return count;
        }

        private void AddList(Transform wrist, Body bod, float prevPos, float currentPos, string side)
        {
            if (!idTimes.ContainsKey(side + bod.bodyID))
                return;

            List<KeyValuePair<float, float>> kvpList = idTimes[side + bod.bodyID];

            float currDistance = 0.0f;

            List<KeyValuePair<float, float>> toRemove = new List<KeyValuePair<float, float>>();

            if (side == "Left")
            {
                //if (bod.LeftElbow.transform.position.y < bod.LeftWrist.transform.position.y)
                    kvpList.Add(new KeyValuePair<float, float>(currentPos - prevPos, 0));
            }
            else
            {
                //if (bod.RightElbow.transform.position.y < bod.RightWrist.transform.position.y)
                    kvpList.Add(new KeyValuePair<float, float>(currentPos - prevPos, 0));
            }


            for (int i = 0; i < kvpList.Count; i++)
            {
                KeyValuePair<float, float> kvp = kvpList[i];
                currDistance += kvp.Key;
                KeyValuePair<float, float> newKvp = new KeyValuePair<float, float>(kvp.Key, kvp.Value + Time.smoothDeltaTime);
                kvpList[i] = newKvp;
                if (newKvp.Value > swipeTime)
                    toRemove.Add(newKvp);
            }

            foreach(KeyValuePair<float, float> kvp in toRemove)
            {
                kvpList.Remove(kvp);
            }

            //if (!idTimes.ContainsKey(side + bod.bodyID))
            //    idTimes.Add(side + bod.bodyID, 0);
            if (timeOut <= 0 && Mathf.Abs(currDistance) > swipeDistance)
            {
                if (currDistance > 0)
                {
                    turner.FlipLeft();
                }
                else
                {
                    turner.FlipRight();
                }
                timeOut = maxTimeOut;
                idTimes[side + bod.bodyID].Clear();
                idTimes.Remove(side + bod.bodyID);
            }

            if(timeSince[side + bod.bodyID] > swipeTime * 2f)
            {
                idTimes.Remove(side + bod.bodyID);
                timeSince.Remove(side + bod.bodyID);
            }
            else
            {
                timeSince[side + bod.bodyID] += Time.smoothDeltaTime;
            }

            //else
            //{
            //    idTimes[side + bod.bodyID] += wrist.position.x - prevPos;
            //}
        }

        /// <summary>
        /// Registers a body for tracking
        /// </summary>
        /// <param name="parent">The parent transform of the body objects</param>
        /// <param name="id">Body ID</param>
        /// <returns>Was the body succesfully registered</returns>
        public bool RegisterBody(Transform parent, ulong id)
        {
            Body newBod = new Body();
            newBod.bodyID = "" + id;

            if (parent.FindChild("ElbowLeft") == null)
                return false;
            if (parent.FindChild("ElbowRight") == null)
                return false;
            if (parent.FindChild("WristLeft") == null)
                return false;
            if (parent.FindChild("WristRight") == null)
                return false;

            foreach (Transform t in parent)
            {
                if (t.name == "ElbowLeft")
                    newBod.LeftElbow = t.gameObject;
                if (t.name == "ElbowRight")
                    newBod.RightElbow = t.gameObject;
                if (t.name == "WristLeft")
                    newBod.LeftWrist = t.gameObject;
                if (t.name == "WristRight")
                    newBod.RightWrist = t.gameObject;
            }

            bodies.Add(newBod);
            return true;
        }

        /// <summary>
        /// Unregisters a body
        /// Could probably do the same by getting the body ID from the parent name, but w/e, this works
        /// </summary>
        /// <param name="parent">Parent transform of the body</param>
        public void UnregisterBody(Transform parent)
        {
            Transform elbowL = parent.FindChild("ElbowLeft");
            foreach (Body b in bodies)
            {
                if (b.LeftElbow.transform == elbowL)
                {
                    bodies.Remove(b);
                    break;
                }
            }
        }
    }
}

/*******
Track each bodies wrists previous location. Add that to body

List of KVP<float, float>. First float distance, second time. Add to each time Time.smoothDeltaTime;
If time float exceeds swipeTime, remove from list
*******/
