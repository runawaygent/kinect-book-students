﻿using UnityEngine;
using System.Collections;

namespace Overly
{
    public class Unsetter : MonoBehaviour
    {
        public Animator anim;
        public MovieTexture mov;
        public GameObject videoInterr;
        public AudioSource a_source;
        public PageTurner turner;

        void Start()
        {
            turner.Turned += Disable;
        }

        public void StartVideoTimeout()
        {
            StartCoroutine(ResetVid());
        }

        public void Disable()
        {
            anim.SetBool("VideoActive", false);
            videoInterr.SetActive(true);
            mov.Stop();
            StopAllCoroutines();
        }

        private IEnumerator ResetVid()
        {
            yield return new WaitForSeconds(2.0f);
            a_source.clip = mov.audioClip;
            a_source.Play();
            mov.Play();
            mov.loop = false;
            yield return new WaitForSeconds(147f);
            anim.SetBool("VideoActive", false);
            videoInterr.SetActive(true);
        }

        void OnDisable()
        {
            
        }
    }
}
