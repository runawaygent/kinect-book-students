﻿using UnityEngine;
using System.Collections;

namespace Overly
{
    public class TimeOuter : MonoBehaviour
    {
        public float endTimeOut;
        public float midTimeOut;
        public PageTurner turner;

        private float timeSinceActivity;
        private int currentIndex;
        private int maxPage;
        // Use this for initialization
        void Start()
        {
            turner.Turned += Turner_Turned;
            maxPage = turner.maxPage;
        }

        private void Turner_Turned()
        {
            currentIndex = turner.GetIndex();
        }

        // Update is called once per frame
        void Update()
        {
            if(timeSinceActivity > (currentIndex == maxPage ? endTimeOut : midTimeOut))
            {
                turner.FlipToStart();
                timeSinceActivity = 0;
            }
            timeSinceActivity += Time.smoothDeltaTime;
        }

        public void ResetTime()
        {
            timeSinceActivity = 0;
        }
    }
}
