﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Overly
{
    public class Viktorina : MonoBehaviour
    {
        [Tooltip("Parent to move the object to when it becomes active")]
        public Transform activeParent;
        [Tooltip("Parent where the object sits while inactive")]
        public Transform inactiveParent;
        [Tooltip("Animator that is responsible for object animations")]
        public Animator anim;
        [Tooltip("The page turner script")]
        public PageTurner p_turn;
        [Tooltip("Animation length for closing")]
        public float animLength;
        [Tooltip("Parent for active interract zones")]
        public Transform interractiveActive;
        [Tooltip("Parent for inactive interract zones")]
        public Transform interractiveInactive;
        [Tooltip("Parent for choice buttons")]
        public Transform gameButtons;
        [Tooltip("Currently animating option holder. UNUSED")]
        public Transform activeOpt;
        [Tooltip("Answer holding transform. UNUSED")]
        public Transform answers;
        [Tooltip("Answer choices")]
        public string[] options;
        [Tooltip("UI texts that display the choices")]
        public Text[] texts;
        [Tooltip("Index of the correct answer in the options array")]
        public int correct;
        [Tooltip("The viktorina driver. Functionality transfered to scriptable objects")]
        public ViktorinaDriver v_drive;
        [Tooltip("Audio source")]
        public AudioSource a_source;
        [Tooltip("Raw image used to show videos")]
        public RawImage r_img;
        [Tooltip("Movie texture. Verbose")]
        public MovieTexture question, answer;
        [Tooltip("Length of the video in seconds")]
        public float q_length, a_length;

        private int _correct;
        private bool activeAnim;
        
        public void SetActive()
        {
            
            transform.SetParent(activeParent);
            anim.SetBool("Active", true);
            p_turn.lockAnim = true;
            interractiveActive.GetChild(0).SetParent(interractiveInactive, false);
            StartCoroutine(EnableActives());
            v_drive.vik = GetComponent<Viktorina>();
            ShuffleOptions();
        }

        private IEnumerator EnableActives()
        {
            yield return new WaitForSeconds(2.0f);
            
        }

        public void StartThing()
        {
            anim.SetTrigger("Start");
            
        }

        public void Reset()
        {
            anim.SetTrigger("End");
            StartCoroutine(WaitToAct());
        }

        private IEnumerator WaitToAct()
        {
            yield return new WaitForSeconds(2.0f);
            foreach (Transform t in interractiveActive.GetChild(0))
            {
                t.gameObject.SetActive(true);
            }
        }

        public void SetInactive()
        {
            if (anim.GetBool("Active"))
            {
                StopAllCoroutines();
                question.Stop();
                answer.Stop();
                anim.SetBool("Active", false);
                StartCoroutine(Inactive());
            }
        }

        private IEnumerator Inactive()
        {
            yield return new WaitForSeconds(animLength);
            interractiveActive.GetChild(0).SetParent(gameButtons, false);
            foreach (Transform t in gameButtons.GetChild(0))
            {
                t.gameObject.SetActive(true);
            }
            transform.SetParent(inactiveParent);
            p_turn.lockAnim = false;
            interractiveInactive.GetChild(0).SetParent(interractiveActive, false);
        }

        public void StartVideo()
        {
            activeAnim = true;
            r_img.texture = question;
            a_source.clip = question.audioClip;
            question.Play();
            a_source.Play();
            StartCoroutine(SetContent(q_length, true));
            gameButtons.GetChild(0).SetParent(interractiveActive, false);
        }

        private IEnumerator SetContent(float waitFor, bool buttons)
        {
            yield return new WaitForSeconds(waitFor);
            if(!buttons)
                SetInactive();
            else
                anim.SetTrigger("Content");
            activeAnim = false;
        }

        public void CorrectAnswer()
        {
            activeAnim = true;
            r_img.texture = answer;
            a_source.clip = answer.audioClip;
            answer.Play();
            a_source.Play();
            StartCoroutine(SetContent(a_length, false));
        }

        public void ShuffleOptions()
        {
            int ind;
            List<string> options = new List<string>(this.options);
            for(int i = 0; i < texts.Length; i++)
            {
                ind = Mathf.FloorToInt(Random.Range(0.0f, options.Count - 0.00001f));
                texts[i].text = options[ind];
                
                if (this.options[correct] == options[ind])
                    _correct = i;

                options.RemoveAt(ind);
            }
        }

        public IEnumerator TestAnswer(int index)
        {
            if (!activeAnim)
            {
                activeAnim = true;
                //texts[index].transform.parent.SetParent(activeOpt);
                Animator an = texts[index].transform.parent.GetComponent<Animator>();
                yield return new WaitForEndOfFrame();
                if (index == _correct)
                {
                    an.SetTrigger("Correct");
                    anim.SetTrigger("Correct");
                    yield return new WaitForSeconds(2.1f);
                    ResetOption();
                    CorrectAnswer();
                }
                else
                {
                    an.SetTrigger("Incorrect");
                    anim.SetTrigger("Incorrect");
                    yield return new WaitForSeconds(2.1f);

                    //interractiveActive.GetChild(0).gameObject.SetActive(true);
                }
                activeAnim = false;
            }
        }

        public void ResetOption()
        {
            if(activeOpt.childCount > 0)
                activeOpt.GetChild(0).SetParent(answers);
        }
    }
}
