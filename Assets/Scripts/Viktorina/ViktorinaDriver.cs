﻿using UnityEngine;
using System.Collections;

namespace Overly
{
    public class ViktorinaDriver : MonoBehaviour
    {
        public Viktorina vik;

        public void CheckAnswer(int index)
        {
            vik.StartCoroutine(vik.TestAnswer(index));
        }
    }
}
