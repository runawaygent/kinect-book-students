﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


namespace Overly {
    /// <summary>
    /// Handles calling the interract Scriptable Object
    /// </summary>
    public class InterractCollider : MonoBehaviour {
        /// <summary>
        /// The asset that implements ColliderAbstract. Used to call various interractions
        /// </summary>
        [Tooltip("Asset that implements ColliderAbstract")]
        public ColliderAbstract colAbs;
        /// <summary>
        /// GameObjects that holds components that the Asset requires
        /// </summary>
        [Tooltip("GameObjects that holds components that the Asset requires")]
        public GameObject[] assignedObjects;
        /// <summary>
        /// Set to true if the interractable zone should act as a button
        /// </summary>
        [Tooltip("Set to true if the interractable zone should act as a button")]
        public bool singleTon;
        /// <summary>
        /// How long the interract zone needs to be held for it to click
        /// </summary>
        [Tooltip("How long the interract zone needs to be held for it to click")]
        public float timeToSingle;
        /// <summary>
        /// Interract progress UI Image
        /// </summary>
        [Tooltip("Interract progress UI Image")]
        public Image prog;
        /// <summary>
        /// How fast should the interract zone progress decays (the bigger the value, the faster the decay)
        /// </summary>
        [Tooltip("How fast should the interract zone progress decays (the bigger the value, the faster the decay)")]
        public float decayTime;
        /// <summary>
        /// Timeouter. Used to reset time since last activity
        /// </summary>
        [Tooltip("Timeouter. Used to reset time since last activity")]
        public TimeOuter timeOut;

        private float timeSince;
        private bool noProg;
        private bool hasHit;
        // Use this for initialization
        void Start() {
            colAbs.StartMe(assignedObjects);
        }

        void Update()
        {
            if (singleTon)
            {
                if (noProg && timeSince > 0 || hasHit)
                {
                    timeSince -= Time.smoothDeltaTime * decayTime;
                    if (timeSince <= 0)
                    {
                        hasHit = false;
                        timeSince = 0;
                    }
                }
                noProg = true;

                if(prog != null)
                    prog.fillAmount = timeSince / timeToSingle;
            }
            colAbs.OnUpdate();
        }

        /// <summary>
        /// Called from the hand objects when the hand is over the interract zone
        /// </summary>
        public void OnHit()
        {
            if (!singleTon)
                colAbs.Continous();
            else {
                if (timeSince < timeToSingle && !hasHit)
                    timeSince += Time.smoothDeltaTime;
                else if(!hasHit)
                {
                    hasHit = true;
                    colAbs.SingleTon();
                }
                noProg = false;
            }

            if(timeOut != null)
                timeOut.ResetTime();
        }
    }
}
