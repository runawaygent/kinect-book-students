﻿using UnityEngine;
using System.Collections;
using Overly;

public class PagePls : MonoBehaviour {
    public PageTurner p_turn;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.LeftArrow) && !p_turn.lockAnim)
        {
            p_turn.FlipRight();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) && !p_turn.lockAnim)
        {
            p_turn.FlipLeft();
        }
	}
}
