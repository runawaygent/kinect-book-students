﻿using UnityEngine;
using System.Collections;

namespace Overly {
    public class Hand : MonoBehaviour {
        public GameObject realHand;
        public GestureRegister greg;

        private Transform colliderParent;
        private Transform bubbleParent;
        private Transform pageTurners;
        private InterractCollider inCol;
        // Use this for initialization
        void Start() {
            colliderParent = transform.parent.GetChild(0);
            bubbleParent = transform.parent.GetChild(1);
            pageTurners = transform.parent.GetChild(2);
        }

        // Update is called once per frame
        void Update() {
            if (colliderParent.childCount > 0)
                foreach (Transform t in colliderParent.GetChild(0))
                {
                    if (AreCoordsWithinUiObject(transform.position, t))
                    {
                        inCol = t.GetComponent<InterractCollider>();
                        if(inCol != null)
                            inCol.OnHit();
                    }
            }

            if (bubbleParent.childCount > 0)
            {
                foreach (Transform t in bubbleParent)
                {
                    if (AreCoordsWithinUiObject(transform.position, t))
                    {
                        inCol = t.GetComponent<InterractCollider>();
                        if (inCol != null)
                            inCol.OnHit();
                    }
                }
            }

            foreach (Transform t in pageTurners)
                if (AreCoordsWithinUiObject(transform.position, t))
                    greg.AddToId(realHand);
        }

        public bool AreCoordsWithinUiObject(Vector2 coords, Transform gameObj)
        {
            Vector2 localPos = gameObj.InverseTransformPoint(coords);
            return ((RectTransform)gameObj).rect.Contains(localPos);
        }

    }
}
