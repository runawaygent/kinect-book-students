﻿using UnityEngine;
using System.Collections;

namespace Overly
{
    public class BubbleSpawner : MonoBehaviour
    {
        public Transform[] targetSpawns;
        public Transform bubbleSpawn;
        public Transform paren, bubbParen;
        public AnimationCurve timeBetween;
        public GameObject[] bubble;
        public GameObject target;
        public AudioSource a_source;
        public ScoreKeeper keeper;

        public void StartSpawn()
        {
            StartCoroutine(Spawn());
        }

        private IEnumerator Spawn()
        {
            float time = 0;
            float spawnTime = 0;
            do
            {
                if (timeBetween.Evaluate(time) < spawnTime)
                {
                    int index = Mathf.FloorToInt(Random.Range(0, targetSpawns.Length - 0.00001f));
                    int bubInd = Mathf.FloorToInt(Random.Range(0, bubble.Length - 0.00001f));

                    GameObject bubbs = Instantiate(bubble[bubInd]);
                    bubbs.transform.SetParent(paren);
                    bubbs.name = time + "";
                    bubbs.transform.position = targetSpawns[index].position;

                    GameObject tar = Instantiate(target);
                    tar.transform.SetParent(paren);
                    tar.transform.position = targetSpawns[index].position;
                    
                    bubbs.GetComponent<Bubbler>().target = tar.transform;
                    bubbs.GetComponent<Bubbler>().trans = bubbParen;
                    bubbs.GetComponent<Bubbler>().a_source = a_source;
                    bubbs.GetComponent<Bubbler>().keeper = keeper;

                    spawnTime = 0;
                    //ToDo: Remove
                    //Destroy(bubbs, 15.0f);
                    Destroy(tar, 16.0f);
                }
                time += Time.smoothDeltaTime;
                spawnTime += Time.smoothDeltaTime;
                yield return null;
            } while (time < 25);
        }
    }
}
