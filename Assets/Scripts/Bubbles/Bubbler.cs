﻿using UnityEngine;
using System.Collections;

namespace Overly
{
    public class Bubbler : MonoBehaviour
    {
        private Vector3 velocity = Vector3.zero;
        public Transform target;
        public GameObject UITrig;
        public LayerMask lmask;
        public Transform trans;
        public AudioSource a_source;
        public AudioClip a_pop;
        public ColliderAbstract colabs;
        public ScoreKeeper keeper;

        private RectTransform rekt;
        private bool popped = false;
        // Use this for initialization
        void Start()
        {
            GameObject go = Instantiate(UITrig);
            go.transform.SetParent(trans);
            go.name = gameObject.name;
            go.GetComponent<InterractCollider>().assignedObjects = new GameObject[2] { gameObject, go };
            go.GetComponent<InterractCollider>().colAbs = ScriptableObject.CreateInstance<BubblePopper>();
            rekt = go.GetComponent<RectTransform>();
        }

        // Update is called once per frame
        void Update()
        {
            if (!popped)
            {
                transform.position = Vector3.SmoothDamp(transform.position, target.position, ref velocity, 0.5f);
                rekt.position = (Vector2)Camera.main.WorldToScreenPoint(transform.position);
            }
            if(transform.position.y < -600)
            {
                Destroy(gameObject);
            }
        }

        public void Pop(bool sound)
        {
            popped = true;
            if (rekt != null)
            {
                Destroy(rekt.gameObject);
            }
            if (sound)
            {
                keeper.AddScore();
                a_source.PlayOneShot(a_pop);
                GetComponent<Renderer>().enabled = false;
                GetComponent<Rigidbody>().useGravity = true;
                GetComponent<Rigidbody>().isKinematic = false;
            }
        }
    }
}
