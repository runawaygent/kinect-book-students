﻿using UnityEngine;
using System.Collections;

namespace Overly
{
    public class BubbleCollider : MonoBehaviour
    {
        public GameObject UI;
        public Transform tarParent;

        void OnTriggerEnter(Collider other)
        {
            //if (other.GetComponent<Bubbler>() != null)
            //{
            //    GameObject go = Instantiate(UI);
            //    other.GetComponent<Bubbler>().UITrig = go;
            //    go.transform.SetParent(tarParent, false);
            //    go.transform.localScale = other.transform.localScale;
            //    go.transform.position = Camera.main.WorldToScreenPoint(other.transform.position);
            //}
        }

        void OnTriggerExit(Collider other)
        {
            if (other.GetComponent<Bubbler>() != null)
            {
                //Destroy(other.GetComponent<Bubbler>().UITrig);
                Destroy(other.gameObject, 0.1f);
                other.GetComponent<Bubbler>().Pop(false);
            }
        }
    }
}
