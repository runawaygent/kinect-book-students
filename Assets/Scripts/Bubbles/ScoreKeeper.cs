﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Overly {
    public class ScoreKeeper : MonoBehaviour {
        public Text score, time, bigTime;
        public float maxTime;
        public BubbleSpawner b_spawn;

        private float timeSince;
        private int p_score;
        private bool started;
        // Use this for initialization
        void Start() {
            timeSince = 0;
        }

        // Update is called once per frame
        void Update() {
            if (started)
            {
                if (timeSince <= maxTime)
                {
                    timeSince += Time.smoothDeltaTime;
                    time.text = string.Format("{0:0}", maxTime - timeSince);
                }
                else
                {
                    time.text = "0";
                }
            }
            else
            {
                time.text = "" + maxTime;
            }
        }

        public void BigCountdown()
        {
            StartCoroutine(BiggySmalls());
        }

        private IEnumerator BiggySmalls()
        {
            bigTime.text = 3 + "";
            yield return new WaitForSeconds(1.0f);
            for (int i = 3; i > 0; i--)
            {
                bigTime.text = i + "";
                yield return new WaitForSeconds(1.0f);
            }
        }

        public void SetUp()
        {
            started = false;
            timeSince = 0;
            
        }

        public void Reset()
        {
            StartCoroutine(ResetText());
        }

        private IEnumerator ResetText()
        {
            while(p_score > 0)
            {
                score.text = --p_score + "";
                yield return null;
            }
        }

        public void StartSpawn()
        {
            
            started = true;
            b_spawn.StartSpawn();
        }

        public void AddScore()
        {
            if (started)
                score.text = "" + ++p_score;
        }
    }
}
