﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BubbleMovement : MonoBehaviour {
    public float startZ;
    public float endZ;
    public float step;
    public Vector2 ranRange;
    public Vector2 maxValues;
    public Vector2 minValues;

    private Queue<Vector3> navLocations = new Queue<Vector3>();
	// Use this for initialization
	void Start () {
        float thisZ = startZ;
        Vector3 location;
        Vector3 lastVector = transform.position;
        bool destReach;
        do
        {

            if (startZ > endZ)
            {
                thisZ -= step;
                destReach = thisZ > endZ;
            }
            else
            {
                thisZ += step;
                destReach = thisZ < endZ;
            }
            Vector2 fudge = Vector2.zero;/*(-(Vector2)lastVector).normalized * (Mathf.Abs(ranRange.x) + Mathf.Abs(ranRange.y));*/
            location = new Vector3(lastVector.x + Random.Range(ranRange.x + fudge.x, ranRange.y + fudge.y), lastVector.y + Random.Range(ranRange.x + fudge.x, ranRange.y + fudge.y), thisZ);
            lastVector = location;
            navLocations.Enqueue(location);
        } while (destReach);

        StartCoroutine(MoveTo());
	}
	
	private IEnumerator MoveTo()
    {
        yield return new WaitForEndOfFrame();
        Vector3 target;
        Vector3 startLocation;
        float time;
        int count = navLocations.Count;
        for (int i = 0; i < count; i++)
        {
            target = navLocations.Dequeue();
            startLocation = transform.position;
            time = 0;
            do
            {
                Debug.DrawLine(transform.position, target, Color.red);
                transform.position = Vector3.Lerp(startLocation, target, time);
                time += Time.smoothDeltaTime;
                yield return null;
            } while (time < 1);
        }
    }
}
