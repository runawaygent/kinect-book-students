﻿using UnityEngine;
using System.Collections;
namespace Overly
{
    public class Body
    {
        /// <summary>
        /// The body ID
        /// </summary>
        public string bodyID;
        /// <summary>
        /// Left wrist gameobject
        /// </summary>
        public GameObject LeftWrist;
        /// <summary>
        /// Right wrist gameobject
        /// </summary>
        public GameObject RightWrist;
        /// <summary>
        /// Left elbow gameobject
        /// </summary>
        public GameObject LeftElbow;
        /// <summary>
        /// Right elbow gameobject
        /// </summary>
        public GameObject RightElbow;
        /// <summary>
        /// previous wrist x location
        /// </summary>
        public float prevLeftLocX;
        /// <summary>
        /// previous wrist y location
        /// </summary>
        public float prevRightLocX;

        /// <summary>
        /// Sets the positions of wrists where they were this frame
        /// </summary>
        public void SetPositions()
        {
            prevLeftLocX = LeftWrist.transform.position.x;
            prevRightLocX = RightWrist.transform.position.x;
        }
    }
}
