﻿using UnityEngine;
using System.Collections;

namespace Overly
{
    public class ButtonBlink : MonoBehaviour
    {
        /// <summary>
        /// Offset from start of game for animation trigger
        /// </summary>
        [Tooltip("Offset from start of game for animation trigger")]
        public float offset;
        /// <summary>
        /// The median time for each random dice used to determine how long until the next blink
        /// </summary>
        [Tooltip("The median time for each random dice used to determine how long until the next blink")]
        public float medianTime;
        /// <summary>
        /// The amplitude of each dice rolled for blink timer
        /// </summary>
        [Tooltip("The amplitude of each dice rolled for blink timer")]
        public float diceAmplitude;
        /// <summary>
        /// The number of dice
        /// </summary>
        [Tooltip("The number of dice")]
        public int diceNum;

        /// <summary>
        /// Current time since last blink
        /// </summary>
        [Tooltip("Current time since last blink")]
        private float time;
        /// <summary>
        /// Next blink timer
        /// </summary>
        [Tooltip("Next blink timer")]
        private float nextTime;
        /// <summary>
        /// Animator that does the blink
        /// </summary>
        [Tooltip("Animator that does the blink")]
        private Animator anim;
        // Use this for initialization
        void Start()
        {
            time = offset;
            CalcNextTime();
            anim = GetComponent<Animator>();
        }

        /// <summary>
        /// Calculates the next blink time
        /// </summary>
        private void CalcNextTime()
        {
            nextTime = 0;
            float minDice, maxDice;
            //Minimum dice roll
            minDice = medianTime - diceAmplitude / 2;
            //Maximum dice roll
            maxDice = medianTime + diceAmplitude / 2;
            //Rolls all the dice
            for(int i = 0; i < diceNum; i++)
            {
                nextTime += Random.Range(minDice, maxDice);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if(time > nextTime)
            {
                time = 0;
                CalcNextTime();
                anim.SetTrigger("Blink");
            }

            time += Time.smoothDeltaTime;
        }
    }
}
