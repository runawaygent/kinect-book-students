﻿using UnityEngine;
using System.Collections;

namespace Overly
{
    public class PageTurner : MonoBehaviour
    {
        /// <summary>
        /// Gameobject holding one of the shadows
        /// </summary>
        [Tooltip("Gameobject holding one of the shadows")]
        public GameObject shadowsRight, shadowsLeft;
        /// <summary>
        /// Transform holding all the inactive page spreads
        /// </summary>
        [Tooltip("Transform holding all the inactive page spreads")]
        public Transform inactivePages;
        /// <summary>
        /// Transform holding the active page spread
        /// </summary>
        [Tooltip("Transform holding the active page spread")]
        public Transform activePages;
        /// <summary>
        /// The current page transform. Used for the turning animation
        /// </summary>
        [Tooltip("The current page transform. Used for the turning animation")]
        public Transform pageThis;
        /// <summary>
        /// The next page transform. Used for the turning animation
        /// </summary>
        [Tooltip("The next page transform. Used for the turning animation")]
        public Transform pageNext;
        /// <summary>
        /// Transform that holds the various interract zones present on a page
        /// </summary>
        [Tooltip("Transform that holds the various interract zones present on a page")]
        public Transform interracts;
        /// <summary>
        /// The current page interract holder
        /// </summary>
        [Tooltip("The current page interract holder")]
        public Transform interractHolder;
        /// <summary>
        /// The animator for page turning
        /// </summary>
        [Tooltip("The animator for page turning")]
        public Animator anim;
        /// <summary>
        /// The minimum page index. Used to grab page transforms from the inactivePages transform
        /// </summary>
        [Tooltip("The minimum page index. Used to grab page transforms from the inactivePages transform")]
        public int minPage;
        /// <summary>
        /// The maximum page index. Used to grab page transforms from the inactivePages transform
        /// </summary>
        [Tooltip("The maximum page index. Used to grab page transforms from the inactivePages transform")]
        public int maxPage;
        /// <summary>
        /// Are the pages currently in animation
        /// </summary>
        [Tooltip("Are the pages currently in animation")]
        public bool lockAnim;
        /// <summary>
        /// The speed used for turning the pages backwards
        /// </summary>
        [Tooltip("The speed used for turning the pages backwards")]
        public float backwardsSpeed;

        /// <summary>
        /// The current page index
        /// </summary>
        [Tooltip("The current page index")]
        private int pageIndex = 0;
        /// <summary>
        /// Current page index holder. Used for turning
        /// </summary>
        [Tooltip("Current page index holder. Used for turning")]
        private int thisInd;

        /// <summary>
        /// Turned event. Called for any extra content on page that needs to be stopped when the page closes 
        /// For example: Audio, Video or a game
        /// </summary>
        public delegate void OnTurn();
        public event OnTurn Turned;

        /// <summary>
        /// Get the current page index
        /// </summary>
        /// <returns>Current page index</returns>
        public int GetIndex()
        {
            return pageIndex;
        }

        void Start()
        {
            //Sets the very first page as the active page
            Transform curr = inactivePages.GetChild(pageIndex);
            curr.GetChild(minPage).SetParent(activePages);
            curr.GetChild(minPage).SetParent(activePages);

            //Activates the right side shadow, deactivates the left side shadow
            shadowsRight.SetActive(true);
            shadowsLeft.SetActive(false);

            //Sets up the page interract zones
            interracts.GetChild(minPage).GetChild(0).SetParent(interractHolder, false);

            //Deactivates the page turner gameobjects
            pageNext.gameObject.SetActive(false);
            pageThis.gameObject.SetActive(false);
        }

        public void FlipRight()
        {
            //If the current page index is less than the maximum page index
            if (pageIndex < maxPage)
            {
                if (!lockAnim)
                {
                    lockAnim = true;
                    
                    //Gets the current index and then increments the pageIndex
                    thisInd = pageIndex++;

                    //If there are listeners for the page turning event, call them
                    if (Turned != null)
                        Turned();

                    //Sets the pages up for the turning animation
                    SetPages();

                    //Starts the animation
                    anim.SetTrigger("Right");

                    StartCoroutine(SetActiveInactive(thisInd));
                }
                else
                {
                    Debug.Log("Locked");
                }
            }
            else
            {
                Debug.Log("Page out of bounds");
            }
        }

        public void FlipLeft()
        {
            //See FlipRight. Works the same but decrements pageIndex instead of incrementing
            if (pageIndex > minPage)
            {
                if (!lockAnim)
                {
                    lockAnim = true;

                    thisInd = pageIndex--;
                    if (Turned != null)
                        Turned();

                    SetPages();

                    anim.SetTrigger("Left");

                    StartCoroutine(SetActiveInactive(thisInd));
                }
                else
                {
                    Debug.Log("Locked");
                }
            }
            else
            {
                Debug.Log("Page out of bounds");
            }
        }

        private void SetPages()
        {
            //Gets the next page and the current page
            Transform next = inactivePages.GetChild(pageIndex);
            Transform curr = activePages;

            //Activates the animated gameobjects
            pageNext.gameObject.SetActive(true);
            pageThis.gameObject.SetActive(true);

            //Deactivates the interract zones
            interractHolder.GetChild(0).SetParent(interracts.GetChild(thisInd), false);

            //Sets the next page spread as the background spread for the animation
            next.GetChild(1).SetParent(pageNext.GetChild(1));
            pageNext.GetChild(1).GetChild(1).SetAsFirstSibling();
            next.GetChild(0).SetParent(pageNext.GetChild(0));
            pageNext.GetChild(0).GetChild(1).SetAsFirstSibling();

            //Sets the current page spread as the animated spread for animation
            curr.GetChild(1).SetParent(pageThis.GetChild(1));
            pageThis.GetChild(1).GetChild(1).SetAsFirstSibling();
            curr.GetChild(0).SetParent(pageThis.GetChild(0));
            pageThis.GetChild(0).GetChild(1).SetAsFirstSibling();
        }

        private IEnumerator SetActiveInactive(int lasInd)
        {
            //Calculates the wait time until the animation finishes based on current animation speed
            float wait = 1.01f / anim.GetFloat("Speed");
            yield return new WaitForSeconds(wait);

            //Activates the current interractable zones
            interracts.GetChild(pageIndex).GetChild(0).SetParent(interractHolder, false);

            //Sets the animation background spread as the active spread
            Transform move = pageNext.GetChild(0).GetChild(0);
            move.SetParent(activePages, false);
            move.SetAsLastSibling();
            move = pageNext.GetChild(1).GetChild(0);
            move.SetParent(activePages, false);
            move.SetAsLastSibling();

            //Sets the moved page spread as inactive
            move = pageThis.GetChild(0).GetChild(0);
            move.SetParent(inactivePages.GetChild(lasInd), false);
            move = pageThis.GetChild(1).GetChild(0);
            move.SetParent(inactivePages.GetChild(lasInd), false);

            //Disables the animation gameobjects
            pageNext.gameObject.SetActive(false);
            pageThis.gameObject.SetActive(false);

            //Sets the corresponding page shadows
            shadowsLeft.SetActive(pageIndex > minPage);
            shadowsRight.SetActive(pageIndex < maxPage);

            //Unlocks the page turner
            lockAnim = false;
        }

        void Update()
        {
            //If Space is hit, turn the book to the first spread
            if (Input.GetKeyDown(KeyCode.Space))
                FlipToStart();
        }

        /// <summary>
        /// Flips to the begining of the book
        /// </summary>
        public void FlipToStart()
        {
            StartCoroutine(FlipMe());
        }

        private IEnumerator FlipMe()
        {
            //Sets the animation speed as the backwardsSpeed
            anim.SetFloat("Speed", backwardsSpeed);
            //While the page index is larger than the smallest index
            while (pageIndex > minPage)
            {
                //Flip left
                FlipLeft();
                yield return new WaitForSeconds(1.2f / anim.GetFloat("Speed"));
                yield return new WaitForEndOfFrame();
            }
            //Sets the animation speed back to 1
            anim.SetFloat("Speed", 1.0f);
        }
    }
    
}

/*************
Have 2 holders and 2 animations
Each opening consists of two pages.
Page order:
One moving, one static in old
One static, one moving in new (i.e. the one under moving is static and vice versa)
Move both pages at the same time, while swapping their sizes
Once transition is done, move pages back to where they should be, have the active spread in its section, inactive ones held below an inactive parent.
NEVER mess with the pages themselves in this part, only move them to parents that move about
*************/