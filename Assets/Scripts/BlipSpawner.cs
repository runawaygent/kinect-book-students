﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Overly
{
    /// <summary>
    /// Spawns the sparkles on the cover
    /// </summary>
    public class BlipSpawner : MonoBehaviour
    {
        public float timeBetween;
        public GameObject blip;

        private float time;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (time > timeBetween)
            {
                GameObject go = Instantiate(blip);
                int index = Mathf.FloorToInt(Random.Range(0.0f, transform.childCount - 0.0001f));
                go.transform.SetParent(transform.GetChild(index), false);
                //go.GetComponent<Image>().color = new Color(Random.value, Random.value, Random.value);
                time = 0;
            }

            time += Time.smoothDeltaTime;
        }
    }
}
