﻿using UnityEngine;
using System.Collections;

namespace Overly
{
    public class GalleryController : MonoBehaviour
    {
        public bool direction;
        public Gallery gal;

        public void OnClick()
        {
            if (direction)
                gal.Left();
            else
                gal.Right();
        }
    }
}
