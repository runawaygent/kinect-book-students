﻿using UnityEngine;
using System.Collections;

namespace Overly
{
    public class GameOpen : MonoBehaviour
    {
        /// <summary>
        /// The active position parent
        /// </summary>
        [Tooltip("The active position parent")]
        public Transform activeParent;
        /// <summary>
        /// Inactive position parent
        /// </summary>
        [Tooltip("Inactive position parent")]
        public Transform inactiveParent;
        /// <summary>
        /// Animator handling opening and closing the game
        /// </summary>
        [Tooltip("Animator handling opening and closing the game")]
        public Animator anim;
        /// <summary>
        /// The page turner script
        /// </summary>
        [Tooltip("The page turner script")]
        public PageTurner p_turn;
        /// <summary>
        /// Length of the closing animation
        /// </summary>
        [Tooltip("Length of the closing animation")]
        public float animLength;
        /// <summary>
        /// Interractive zone active parent
        /// </summary>
        [Tooltip("Interractive zone active parent")]
        public Transform interractiveActive;
        /// <summary>
        /// Interractive zone inactive parent
        /// </summary>
        [Tooltip("Interractive zone inactive parent")]
        public Transform interractiveInactive;
        /// <summary>
        /// Interractable button parent
        /// </summary>
        [Tooltip("Interractable button parent")]
        public Transform gameButtons;
        /// <summary>
        /// The score keeper
        /// </summary>
        [Tooltip("The score keeper")]
        public ScoreKeeper keeper;
        /// <summary>
        /// The bubble spawner
        /// </summary>
        [Tooltip("The bubble spawner")]
        public BubbleSpawner spawner;

        public void SetActive()
        {
            transform.SetParent(activeParent);
            anim.SetBool("Active", true);
            p_turn.lockAnim = true;
            interractiveActive.GetChild(0).SetParent(interractiveInactive, false);
            StartCoroutine(EnableActives());
            keeper.SetUp();
            keeper.Reset();
            p_turn.Turned += SetInactive;
        }

        private IEnumerator EnableActives()
        {
            yield return new WaitForSeconds(2.0f);
            gameButtons.GetChild(0).SetParent(interractiveActive, false);
        }

        public void StartThing()
        {
            anim.SetTrigger("Start");
            StartCoroutine(TimeOut());
            keeper.Reset();
        }

        public void Reset()
        {
            keeper.SetUp();
            anim.SetTrigger("End");
            StartCoroutine(WaitToAct());
        }

        private IEnumerator WaitToAct()
        {
            yield return new WaitForSeconds(2.0f);
            foreach (Transform t in interractiveActive.GetChild(0))
            {
                t.gameObject.SetActive(true);
            }
        }

        public void SetInactive()
        {
            p_turn.Turned -= SetInactive;
            StopAllCoroutines();
            spawner.StopAllCoroutines();
            anim.SetBool("Active", false);
            StartCoroutine(Inactive());
        }

        private IEnumerator Inactive()
        {
            yield return new WaitForSeconds(animLength);
            interractiveActive.GetChild(0).SetParent(gameButtons, false);
            foreach(Transform t in gameButtons.GetChild(0))
            {
                t.gameObject.SetActive(true);
            }
            transform.SetParent(inactiveParent);
            p_turn.lockAnim = false;
            interractiveInactive.GetChild(0).SetParent(interractiveActive, false);
        }

        private IEnumerator TimeOut()
        {
            yield return new WaitForSeconds(36.0f);
            Reset();
        }
    }
}
