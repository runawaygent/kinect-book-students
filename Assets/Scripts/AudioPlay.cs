﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Overly
{
    public class AudioPlay: MonoBehaviour
    {
        /// <summary>
        /// Audio source
        /// </summary>
        [Tooltip("The audio source for the scene. Should be on the main camera (change if it isn't)")]
        public AudioSource a_source;
        /// <summary>
        /// Associated clip
        /// </summary>
        [Tooltip("The audio clip to play")]
        public AudioClip a_clip;
        [Tooltip("Page turner script")]
        public PageTurner p_turn;
        [Tooltip("Waveform image of the audio clip. Should replace with something that generates it")]
        public Image waveform;

        private Animator anim;
        void Start()
        {
            anim = GetComponent<Animator>();
            p_turn.Turned += P_turn_Turned;
        }

        void Update()
        {
            if (a_source.clip == a_clip)
                waveform.fillAmount = a_source.time / a_clip.length;
            else
                waveform.fillAmount = 0f;

            anim.SetBool("AudioPlaying", a_source.clip == a_clip && a_source.isPlaying);
        }

        private void P_turn_Turned()
        {
            a_source.Stop();
        }

        public void PlayAudio()
        {
            if (a_source.isPlaying)
                a_source.Stop();
            else
            {
                a_source.clip = a_clip;
                a_source.Play();
            }
        }


    }
}
