﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

namespace Overly
{
    public class HandOff : MonoBehaviour
    {
        /// <summary>
        /// The prefab of the visible cursor
        /// </summary>
        [Tooltip("The prefab of the visible cursor")]
        public GameObject handPrefab;
        /// <summary>
        /// The heirarchy parent of cursors
        /// </summary>
        [Tooltip("The heirarchy parent of cursors")]
        public Transform handParent;
        /// <summary>
        /// Gesture registrer
        /// </summary>
        [Tooltip("Gesture registrer")]
        public GestureRegister greg;
        /// <summary>
        /// Multiplyer for world-to-screen position
        /// </summary>
        [Tooltip("Multiplyer for world-to-screen position")]
        public float xMult = 2f, yMult = 2f;

        private RectTransform rekt;
        private Transform elbow;
        private Vector2 kinectCoordinate;
        private Vector2 pixelsPerMeter;
        // Use this for initialization
        void Start()
        {
            GameObject go = Instantiate(handPrefab);
            go.transform.SetParent(handParent, false);
            rekt = go.GetComponent<RectTransform>();
            Hand h = go.GetComponent<Hand>();
            //h.realHand = gameObject;
            h.greg = greg;

            string name = gameObject.name;
            string lookupString;
            string handString;
            if (name.Contains("Left"))
            {
                lookupString = "ElbowLeft";
                handString = "WristLeft";
            }
            else
            {
                lookupString = "ElbowRight";
                handString = "WristRight";
            }

            foreach (Transform t in transform.parent)
            {
                if (t.name == lookupString)
                {
                    elbow = t;
                }
                if (t.name == handString)
                    h.realHand = t.gameObject;
            }

            //string[] lines = File.ReadAllLines("screenConf.txt");

            //Dictionary<string, float> fileDict = new Dictionary<string, float>();
            //for(int i = 0; i < lines.Length; i++)
            //{
            //    string[] tmp = lines[i].Split('=');
            //    fileDict.Add(tmp[0], float.Parse(tmp[1]));
            //}

            //kinectCoordinate = new Vector2(fileDict["kinectFromGround"] - fileDict["screenFromGround"], fileDict["kinectFromLeft"]);
            //pixelsPerMeter = new Vector2(fileDict["screenPixelWidth"] / fileDict["screenWidth"], fileDict["screenPixelHeight"] / fileDict["screenHeight"]);
        }

        // Update is called once per frame
        void Update()
        {
            //I tried to create a thing that calculates where on the screen is a person pointing.
            //I failed.
            //Maybe someone else can figure this out
            
            //Vector2 handXY = transform.position / 10.0f;
            //Vector2 elbowXY = elbow.position / 10.0f;

            //float distance = elbow.position.z / 10.0f;
            //float angle = Vector3.Angle(elbowXY, handXY);
            //float dist = Mathf.Tan(angle * Mathf.Deg2Rad) * distance;

            //handXY += kinectCoordinate;
            //elbowXY += kinectCoordinate;

            //Vector2 normalised = (handXY - elbowXY).normalized;
            //normalised = normalised * dist + elbowXY;

            //rekt.anchoredPosition = new Vector2(normalised.x * pixelsPerMeter.x, normalised.y * pixelsPerMeter.y);

            //Just takes the position fed back by kinect and converts it into screen coordinates
            Vector3 toScreen = new Vector3(transform.position.x * xMult, transform.position.y * yMult, transform.position.z);
            Vector2 screenPos = Camera.main.WorldToScreenPoint(toScreen);
            rekt.anchoredPosition = screenPos;
            //rekt.position = screenPos;
        }

        void OnDestroy()
        {
            if (rekt != null)
                Destroy(rekt.gameObject);
        }

        void OnDisable()
        {
            if (rekt != null)
            {
                rekt.gameObject.SetActive(false);
            }
        }

        void OnEnable()
        {
            if (rekt != null)
            {
                rekt.gameObject.SetActive(true);
            }
        }
    }
}

/******
Get distance from Kinect, get angle between hand and elbow. 
Vector from XY? Yeah, get normalised vector from the XY difference between elbow and hand.
Use Elbow XY as Canvas XY, add normalised vector * dist
******/
