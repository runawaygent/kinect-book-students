﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Overly
{
    public class ViktorinaIdle : MonoBehaviour
    {
        public Sprite[] vidPrev;
        public Image holder1, holder2;
        public float changeTime;

        private float c_time;
        private int index = 0;
        private bool oneOrTwo;
        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            c_time += Time.smoothDeltaTime;
            if(c_time > changeTime)
            {
                if (++index >= vidPrev.Length)
                    index = 0;

                if (oneOrTwo)
                {
                    holder1.sprite = vidPrev[index];
                }
                else
                {
                    holder2.sprite = vidPrev[index];
                }

                StartCoroutine(Swap());

                c_time = 0;
            }
        }

        void OnEnable()
        {
            c_time = 0;
            index = 0;
            holder1.sprite = vidPrev[index];
            holder1.color = Color.white;
            holder2.color = new Color(1,1,1,0);
            oneOrTwo = false;
        }

        private IEnumerator Swap()
        {
            Color one = holder1.color;
            Color two = holder2.color;
            float t = 0.0f;
            while (t < 1.0f)
            {
                one.a = oneOrTwo ? t : 1 - t;
                two.a = oneOrTwo ? 1 - t : t;
                holder1.color = one;
                holder2.color = two;
                t += Time.smoothDeltaTime;
                yield return null;
            }
            oneOrTwo = !oneOrTwo;
        }
    }
}
