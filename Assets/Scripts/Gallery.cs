﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Overly
{
    public class Gallery : MonoBehaviour
    {
        /// <summary>
        /// Array holding the gallery images
        /// </summary>
        [Tooltip("Array holding the gallery images")]
        public Sprite[] sprites;
        /// <summary>
        /// UI image holding gallery images
        /// </summary>
        [Tooltip("UI image holding gallery images")]
        public Image i_this, i_next;
        /// <summary>
        /// Animator that animates the transition
        /// </summary>
        [Tooltip("Animator that animates the transition")]
        public Animator anim;
        /// <summary>
        /// Animation time
        /// </summary>
        [Tooltip("Animation time")]
        public float animTime;

        /// <summary>
        /// Current image index
        /// </summary>
        [Tooltip("Current image index")]
        private int index;
        /// <summary>
        /// Is the animation in progress
        /// </summary>
        [Tooltip("Is the animation in progress")]
        private bool working;

        /// <summary>
        /// Move gallery left
        /// </summary>
        public void Left()
        {
            if (working)
                return;
            working = true;
            if (++index >= sprites.Length)
            {
                index = 0;
            }
            i_next.sprite = sprites[index];
            StartCoroutine(SetNewImages());
            anim.SetTrigger("Left");
        }

        /// <summary>
        /// Move gallery right
        /// </summary>
        public void Right()
        {
            if (working)
                return;

            working = true;
            if (--index < 0)
            {
                index = sprites.Length - 1;
            }
            i_next.sprite = sprites[index];
            StartCoroutine(SetNewImages());
            anim.SetTrigger("Right");
        }

        /// <summary>
        /// Sets the new image after the animation finishes
        /// </summary>
        /// <returns></returns>
        private IEnumerator SetNewImages()
        {
            yield return new WaitForSeconds(animTime);
            i_this.sprite = i_next.sprite;
            working = false;
        }
    }
}
