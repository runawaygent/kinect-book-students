﻿using UnityEngine;
using System.Collections;
using System;

namespace Overly
{
    [CreateAssetMenu(fileName = "Option Collider", menuName = "Overly/Viktorina Option")]
    public class ViktorinaOption : ColliderAbstract
    {
        public int option;

        private ViktorinaDriver v_drive;
        public override void Continous()
        {
            throw new NotImplementedException();
        }

        public override void OnUpdate()
        {
        }

        public override void SingleTon()
        {
            v_drive.CheckAnswer(option);
        }

        public override void StartMe(GameObject[] go)
        {
            v_drive = go[0].GetComponent<ViktorinaDriver>();
        }
    }
}
