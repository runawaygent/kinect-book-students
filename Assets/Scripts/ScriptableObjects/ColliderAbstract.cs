﻿using UnityEngine;
using System.Collections;

namespace Overly
{
    public abstract class ColliderAbstract : ScriptableObject
    {
        public abstract void StartMe(GameObject[] go);
        public abstract void Continous();
        public abstract void SingleTon();
        public abstract void OnUpdate();
    }
}
