﻿using UnityEngine;
using System.Collections;
using System;

namespace Overly
{
    [CreateAssetMenu(fileName = "Spelet", menuName = "Overly/Spelet", order = 1)]
    public class SpeletScript : ColliderAbstract
    {
        private GameOpen gopen;
        private GameObject assignee;
        public override void Continous()
        {
            throw new NotImplementedException();
        }

        public override void OnUpdate()
        {

        }

        public override void SingleTon()
        {
            gopen.StartThing();
            assignee.SetActive(false);
        }

        public override void StartMe(GameObject[] go)
        {
            gopen = go[0].GetComponent<GameOpen>();
            assignee = go[1];
        }
    }
}
