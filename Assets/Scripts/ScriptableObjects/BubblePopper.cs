﻿using UnityEngine;
using System.Collections;
using System;

namespace Overly
{
    [CreateAssetMenu(fileName = "Bubble Popper", menuName = "Overly/Bubble Popper")]
    public class BubblePopper : ColliderAbstract
    {
        private Bubbler b_do;
        private Transform t_one;

        public override void Continous()
        {
            throw new NotImplementedException();
        }

        public override void OnUpdate()
        {
            //Debug.DrawLine(t_one.position, b_do.transform.position, Color.blue);
        }

        public override void SingleTon()
        {
            b_do.Pop(true);
        }

        public override void StartMe(GameObject[] go)
        {
            b_do = go[0].GetComponent<Bubbler>();
            t_one = go[1].GetComponent<Transform>();
        }
    }
}
