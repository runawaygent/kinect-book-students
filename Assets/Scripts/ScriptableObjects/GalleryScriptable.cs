﻿using UnityEngine;
using System.Collections;
using System;

namespace Overly
{
    [CreateAssetMenu(fileName = "Gallery", menuName = "Overly/Gallery")]
    public class GalleryScriptable : ColliderAbstract
    {
        public bool direction;

        private Gallery cont;
        public override void Continous()
        {
        }

        public override void OnUpdate()
        {
        }

        public override void SingleTon()
        {
            if (direction)
                cont.Left();
            else
                cont.Right();
        }

        public override void StartMe(GameObject[] go)
        {
            cont = go[0].GetComponent<Gallery>();
        }
    }
}
