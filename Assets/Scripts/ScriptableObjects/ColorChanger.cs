﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

namespace Overly
{
    [CreateAssetMenu(fileName = "Color Changer", menuName = "Overly/Color Changer")]
    public class ColorChanger : ColliderAbstract
    {
        public Image img;
        public Gradient grad;
        public float decayAmount;

        private float time = 0;
        private bool collided;
        public override void Continous()
        {
            collided = true;
            if (time > 10)
                time = 0;

            img.color = grad.Evaluate(time / 10f);
            time += Time.smoothDeltaTime;

            Debug.Log("Derp");
        }

        public override void SingleTon()
        {
            Debug.Log("Singleton");
        }

        public override void StartMe(GameObject[] go)
        {
            img = go[0].GetComponent<Image>();
        }

        public override void OnUpdate()
        {
            if(time > 0 && !collided)
            {
                img.color = grad.Evaluate(time / 10f);
                time -= Time.smoothDeltaTime * decayAmount;
                if (time < 0)
                    time = 0;
            }
            collided = false;
        }
    }
}
