﻿using UnityEngine;
using System.Collections;
using System;

namespace Overly
{
    [CreateAssetMenu(menuName = "Overly/FlipToStart")]
    public class ToStart : ColliderAbstract
    {
        private PageTurner turner;
        public override void Continous()
        {
            throw new NotImplementedException();
        }

        public override void OnUpdate()
        {
        }

        public override void SingleTon()
        {
            turner.FlipToStart();
        }

        public override void StartMe(GameObject[] go)
        {
            turner = go[0].GetComponent<PageTurner>();
        }
    }
}
