﻿using UnityEngine;
using System.Collections;
using System;

namespace Overly
{
    [CreateAssetMenu(fileName = "Bubble Start", menuName = "Overly/Bubble Starter")]
    public class BubbleGameStart : ColliderAbstract
    {
        private GameOpen gopen;
        private BubbleSpawner bspawn;
        public override void Continous()
        {
        }

        public override void OnUpdate()
        {
        }

        public override void SingleTon()
        {
            gopen.SetActive();
            //bspawn.StartSpawn();
        }

        public override void StartMe(GameObject[] go)
        {
            gopen = go[0].GetComponent<GameOpen>();
            bspawn = go[1].GetComponent<BubbleSpawner>();
        }
    }
}
