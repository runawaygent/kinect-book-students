﻿using UnityEngine;
using System.Collections;
using System;

namespace Overly
{
    [CreateAssetMenu(fileName = "Bubble Closer", menuName = "Overly/Bubble Close")]
    public class CloseBubbles : ColliderAbstract
    {
        private GameOpen gop;
        public override void Continous()
        {
            throw new NotImplementedException();
        }

        public override void OnUpdate()
        {
        }

        public override void SingleTon()
        {
            gop.SetInactive();
        }

        public override void StartMe(GameObject[] go)
        {
            gop = go[0].GetComponent<GameOpen>();
        }
    }
}
