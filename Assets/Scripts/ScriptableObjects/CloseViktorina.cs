﻿using UnityEngine;
using System.Collections;
using System;

namespace Overly
{
    [CreateAssetMenu(fileName = "Viktorina Closer", menuName = "Overly/Viktorina Close")]
    public class CloseViktorina : ColliderAbstract
    {
        private Viktorina[] gop;
        public override void Continous()
        {
            throw new NotImplementedException();
        }

        public override void OnUpdate()
        {
        }

        public override void SingleTon()
        {
            foreach (Viktorina v in gop)
                v.SetInactive();
        }

        public override void StartMe(GameObject[] go)
        {
            gop = new Viktorina[go.Length];
            for (int i = 0; i < go.Length; i++)
                gop[i] = go[i].GetComponent<Viktorina>();
        }
    }
}
