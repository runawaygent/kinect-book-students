﻿using UnityEngine;
using System.Collections;
using System;

namespace Overly
{
    [CreateAssetMenu(menuName = "Overly/Audio")]
    public class AudioPlayer : ColliderAbstract
    {
        private AudioPlay a_play;
        public override void Continous()
        {
            throw new NotImplementedException();
        }

        public override void OnUpdate()
        {
        }

        public override void SingleTon()
        {
            a_play.PlayAudio();
        }

        public override void StartMe(GameObject[] go)
        {
            a_play = go[0].GetComponent<AudioPlay>();
        }
    }
}
