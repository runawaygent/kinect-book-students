﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

namespace Overly
{
    [CreateAssetMenu(fileName = "Video Trigger",menuName = "Overly/Video Trigger",order = 0)]
    public class VideoTrigger : ColliderAbstract
    {
        public bool enabler;
        public MovieTexture movTex;

        private Unsetter unset;
        private Animator anim;
        private RawImage raw;
        private GameObject activator;
        private GameObject disabler;
        public override void Continous()
        {
            throw new NotImplementedException();
        }

        public override void OnUpdate()
        {
        }

        public override void SingleTon()
        {
            if (enabler)
            {
                activator.SetActive(false);
                disabler.SetActive(true);
                anim.SetBool("VideoActive", true);
                unset.StartVideoTimeout();
            }
            else
            {
                unset.Disable();
                activator.SetActive(true);
                disabler.SetActive(false);
            }
        }

        public override void StartMe(GameObject[] go)
        {
            anim = go[0].GetComponent<Animator>();
            raw = go[1].GetComponent<RawImage>();
            activator = go[2];
            unset = go[3].GetComponent<Unsetter>();
            disabler = go[4];
        }
    }
}
