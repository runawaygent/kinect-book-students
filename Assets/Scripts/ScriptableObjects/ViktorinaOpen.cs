﻿using UnityEngine;
using System.Collections;
using System;

namespace Overly
{
    [CreateAssetMenu(menuName = "Overly/Viktorina Open")]
    public class ViktorinaOpen : ColliderAbstract
    {
        private Viktorina vik;
        public override void Continous()
        {
            throw new NotImplementedException();
        }

        public override void OnUpdate()
        {
            
        }

        public override void SingleTon()
        {
            vik.SetActive();
        }

        public override void StartMe(GameObject[] go)
        {
            vik = go[0].GetComponent<Viktorina>();
        }
    }
}
