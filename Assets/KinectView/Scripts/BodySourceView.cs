﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using Overly;

public class BodySourceView : MonoBehaviour 
{
    //This is a kinect class that came as an example for kinect implementation
    //I've barely modified it, but it basically spawns objects for each skeleton joint
    //And tracks the objects along the joints.
    //I've only added logic that adds my components to them, used to track cursors and page turning
    public Material BoneMaterial;
    public GameObject BodySourceManager;
    public GestureRegister greg;
    public GameObject handRight, handLeft;
    public Transform handParent;
    
    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;
    private Kinect.HandState hstate;
    
    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },

        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },

        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },
        
        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },

        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };
    
    void Update () 
    {
        if (BodySourceManager == null)
        {
            return;
        }
        
        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {
            return;
        }
        
        Kinect.Body[] data = _BodyManager.GetData();
        if (data == null)
        {
            return;
        }
        
        List<ulong> trackedIds = new List<ulong>();
        foreach(var body in data)
        {
            if (body == null)
            {
                continue;
              }
                
            if(body.IsTracked)
            {
                trackedIds.Add (body.TrackingId);
            }
        }
        
        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);
        
        // First delete untracked bodies
        foreach(ulong trackingId in knownIds)
        {
            if(!trackedIds.Contains(trackingId))
            {
                greg.UnregisterBody(_Bodies[trackingId].transform);
                Destroy(_Bodies[trackingId]);
                _Bodies.Remove(trackingId);
            }
        }

        foreach(var body in data)
        {
            if (body == null)
            {
                continue;
            }
            
            if(body.IsTracked)
            {
                if(!_Bodies.ContainsKey(body.TrackingId))
                {
                    _Bodies[body.TrackingId] = CreateBodyObject(body);
                    if(!greg.RegisterBody(_Bodies[body.TrackingId].transform, body.TrackingId))
                        Debug.LogError("Body not registered!");
                }
                
                RefreshBodyObject(body, _Bodies[body.TrackingId]);
            }
        }
    }
    
    private GameObject CreateBodyObject(Kinect.Body bod)
    {
        GameObject body = new GameObject("Body:" + bod.TrackingId);
        
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            if (jt == Kinect.JointType.HandLeft ||
                jt == Kinect.JointType.HandRight ||
                jt == Kinect.JointType.ElbowLeft ||
                jt == Kinect.JointType.ElbowRight ||
                jt == Kinect.JointType.WristLeft ||
                jt == Kinect.JointType.WristRight)
            {
                GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                if (jt == Kinect.JointType.HandLeft)
                {
                    HandOff h = jointObj.AddComponent<HandOff>();
                    h.handPrefab = handLeft;
                    h.handParent = handParent;
                    h.greg = greg;
                    jointObj.GetComponent<Renderer>().material.color = Color.green;

                }
                if(jt == Kinect.JointType.HandRight)
                {
                    HandOff h = jointObj.AddComponent<HandOff>();
                    h.handPrefab = handRight;
                    h.handParent = handParent;
                    h.greg = greg;
                    jointObj.GetComponent<Renderer>().material.color = Color.green;
                }
                jointObj.GetComponent<Renderer>().enabled = false;
                jointObj.GetComponent<BoxCollider>().enabled = false;
                //LineRenderer lr = jointObj.AddComponent<LineRenderer>();
                //lr.SetVertexCount(2);
                //lr.material = BoneMaterial;
                //lr.SetWidth(0.05f, 0.05f);

                jointObj.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
                jointObj.name = jt.ToString();
                jointObj.transform.parent = body.transform;
            }
        }
        
        return body;
    }
    
    private void RefreshBodyObject(Kinect.Body body, GameObject bodyObject)
    {
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            if (jt == Kinect.JointType.HandLeft ||
                jt == Kinect.JointType.HandRight ||
                jt == Kinect.JointType.ElbowLeft ||
                jt == Kinect.JointType.ElbowRight ||
                jt == Kinect.JointType.WristLeft ||
                jt == Kinect.JointType.WristRight)
            {
                Kinect.Joint sourceJoint = body.Joints[jt];
                Kinect.Joint? targetJoint = null;

                if (_BoneMap.ContainsKey(jt))
                {
                    targetJoint = body.Joints[_BoneMap[jt]];
                }

                Transform jointObj = bodyObject.transform.FindChild(jt.ToString());
                jointObj.localPosition = GetVector3FromJoint(sourceJoint);

                jointObj.gameObject.SetActive(sourceJoint.Position.Z < 2f);

                //LineRenderer lr = jointObj.GetComponent<LineRenderer>();
                //if (targetJoint.HasValue)
                //{
                //    lr.SetPosition(0, jointObj.localPosition);
                //    lr.SetPosition(1, GetVector3FromJoint(targetJoint.Value));
                //    lr.SetColors(GetColorForState(sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
                //}
                //else
                //{
                //    lr.enabled = false;
                //}
            }
        }
    }
    
    private static Color GetColorForState(Kinect.TrackingState state)
    {
        switch (state)
        {
        case Kinect.TrackingState.Tracked:
            return Color.green;

        case Kinect.TrackingState.Inferred:
            return Color.red;

        default:
            return Color.black;
        }
    }
    
    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {
        return new Vector3(joint.Position.X * 10, joint.Position.Y * 10, joint.Position.Z * 10);
    }
}
