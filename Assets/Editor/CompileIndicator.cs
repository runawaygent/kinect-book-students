using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

// I recommend dropping this script in an Editor folder.
// You should have two audio clips in a Resources folder.
// You'll need to put the names of those clips in the static initializer below.

namespace Assets.Editor {

	/// <summary>
	/// Plays a sound effect when script compiling starts and ends. 
	/// </summary>
	[InitializeOnLoad]
	public static class CompileIndicator {

		private const string CompileStatePrefsKey = "CompileIndicator.WasCompiling";
		private static readonly AudioClip StartClip;
		private static readonly AudioClip EndClip;

		static CompileIndicator() {
			EditorApplication.update = OnUpdate;
			StartClip = Resources.Load<AudioClip>("start");
			EndClip = Resources.Load<AudioClip>("end");
		}

		private static void OnUpdate() {
			var wasCompiling = EditorPrefs.GetBool(CompileStatePrefsKey);
			var isCompiling = EditorApplication.isCompiling;
			if (!wasCompiling && isCompiling) {
				OnStartCompiling();
			} else if (wasCompiling && !isCompiling) {
				OnEndCompiling();
			}
			EditorPrefs.SetBool(CompileStatePrefsKey, isCompiling);
		}

		private static void OnStartCompiling() {
			PlayClip(StartClip);
		}

		private static void OnEndCompiling() {
			PlayClip(EndClip);
		}

		private static void PlayClip(AudioClip clip) {
			Assembly unityEditorAssembly = typeof(AudioImporter).Assembly;
			Type audioUtilClass = unityEditorAssembly.GetType("UnityEditor.AudioUtil");
			MethodInfo method = audioUtilClass.GetMethod(
				"PlayClip",
				BindingFlags.Static | BindingFlags.Public,
				null,
				new []{typeof(AudioClip)},
				null
			);
			method.Invoke(null, new object[]{clip});
		}
	}
}